REBAR = $(shell pwd)/rebar3
APP=snarl

.PHONY: stagedevrel version all tree

all: version compile

include fifo.mk

version_header:
	@./version_header.sh

clean:
	$(REBAR) clean
	make -C rel/pkg clean

long-test:
	$(REBAR) as eqc,long eunit
